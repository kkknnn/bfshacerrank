#include <bits/stdc++.h>
#include <list>

using namespace std;

vector<string> split_string(string);

// Complete the bfs function below.
vector<int> bfs(int n, int m, vector<vector<int>> edges, int s)
{

std::vector<int> tosearch;
std::vector<bool> visitednodes;
std::vector<int> distances;

for (int i = 0; i<n; i++)
{
    distances.push_back(-1);
}
for (int i = 0; i<n; i++)
{
    visitednodes.push_back(false);
}

distances[s-1]=0;
tosearch.push_back(s);
int searchnode;
while(!tosearch.empty())
{
auto beg = tosearch.begin();
searchnode=*beg;
visitednodes[searchnode-1]=true;
tosearch.erase(beg);

for(int i = 0; i<edges.size(); i++)
{
if (edges[i][0]==searchnode)
{
int connectedpoint = edges[i][1];
if (!visitednodes[connectedpoint-1])
{
tosearch.push_back(connectedpoint);
if (distances[connectedpoint-1]==-1)
    {
        visitednodes[connectedpoint-1]=true;
        distances[connectedpoint-1]=distances[searchnode-1];
    }
distances[connectedpoint-1]+=6;
}
}
else if (edges[i][1]==searchnode)
{
int connectedpoint = edges[i][0];
if (!visitednodes[connectedpoint-1])
{
tosearch.push_back(connectedpoint);
if (distances[connectedpoint-1]==-1)
    {
        visitednodes[connectedpoint-1]=true;
        distances[connectedpoint-1]=distances[searchnode-1];
    }
distances[connectedpoint-1]+=6;
}
}
}
}

auto removestartingpoint = find(distances.begin(),distances.end(),0);
distances.erase(removestartingpoint);

return distances;


}




int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int q;
    cin >> q;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int q_itr = 0; q_itr < q; q_itr++) {
        string nm_temp;
        getline(cin, nm_temp);

        vector<string> nm = split_string(nm_temp);

        int n = stoi(nm[0]);

        int m = stoi(nm[1]);

        vector<vector<int>> edges(m);
        for (int i = 0; i < m; i++) {
            edges[i].resize(2);

            for (int j = 0; j < 2; j++) {
                cin >> edges[i][j];
            }

            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }

        int s;
        cin >> s;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        vector<int> result = bfs(n, m, edges, s);

        for (int i = 0; i < result.size(); i++) {
            fout << result[i];

            if (i != result.size() - 1) {
                fout << " ";
            }
        }

        fout << "\n";
    }

    fout.close();

    return 0;
}

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}


